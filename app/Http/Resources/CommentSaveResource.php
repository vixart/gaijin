<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentSaveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author' => $this->author,
            'text' => $this->text,
            'depth' => $this->depth,
            'parentId' => $this->parent_id,
            'createdAt' => $this->created_at,
            'children' => CommentSaveResource::collection($this->children)
        ];
    }
}
