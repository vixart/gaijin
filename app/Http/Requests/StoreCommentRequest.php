<?php

namespace App\Http\Requests;

use Comments\Entities\Requests\StoreCommentRQ;
use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'text' => 'required|string',
            'author' => 'required|string',
            'parentId' => 'nullable|integer'
        ];
    }

    public function extractEntity(): StoreCommentRQ
    {
        $entity = new StoreCommentRQ();
        $entity->setText($this->text);
        $entity->setAuthor($this->author);
        $entity->setParentId($this->parentId);

        return $entity;
    }
}
