<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Middleware\ForceJsonResponse;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentSaveResource;
use Comments\Repositories\CommentsRepo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CommentsController extends Controller
{
    private $commentsRepo;

    public function __construct(CommentsRepo $commentsRepo)
    {
        $this->middleware(ForceJsonResponse::class);
        $this->commentsRepo = $commentsRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return CommentSaveResource::collection($this->commentsRepo->getTree());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCommentRequest $request
     * @return false|string
     */
    public function store(StoreCommentRequest $request)
    {
        $entity = $request->extractEntity();
        return new CommentSaveResource($this->commentsRepo->store($entity));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCommentRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateCommentRequest $request, $id)
    {
        $data = $request->validated();
        $result = $this->commentsRepo->update($id, $data);
        return response()->json($result, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $result = $this->commentsRepo->delete($id);
        return response()->json($result, 200);
    }
}
