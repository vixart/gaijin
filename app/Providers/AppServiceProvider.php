<?php

namespace App\Providers;

use Comments\Repositories\CommentsRepo;
use Illuminate\Support\ServiceProvider;
use Comments\Repositories\Implementations\CommentsRepo as CommentsRepoImpl;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(CommentsRepo::class, CommentsRepoImpl::class);
    }
}
