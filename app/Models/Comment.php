<?php

namespace App\Models;

use App\Models\Implementations\CommentImpl;
use Illuminate\Database\Eloquent\Model;
use Comments\Entities\Comment as CommentInterface;
use Kalnoy\Nestedset\NodeTrait;

class Comment extends Model implements CommentInterface
{
    use CommentImpl, NodeTrait;
    protected $table = 'comments';
    protected $guarded = ['id'];
}
