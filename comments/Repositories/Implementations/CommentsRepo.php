<?php

namespace Comments\Repositories\Implementations;
use Comments\Entities\Comment;
use App\Models\Comment as CommentModel;
use Comments\Entities\Requests\StoreCommentRQ;
use Comments\Repositories\CommentsRepo as CommentsRepoInterface;

class CommentsRepo implements CommentsRepoInterface
{
    public function store(StoreCommentRQ $data): Comment
    {
        return CommentModel::create($data->asArray());
    }

    public function update(int $id, array $data): bool
    {
        return CommentModel::where('id', $id)
            ->update($data);
    }

    public function delete(int $id): bool
    {
        return CommentModel::destroy($id);
    }

    public function getTree(): object
    {
        return CommentModel::withDepth()->get()->toTree();
    }
}
