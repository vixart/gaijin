<?php

namespace Comments\Repositories;

use Comments\Entities\Comment;
use Comments\Entities\Requests\StoreCommentRQ;

interface CommentsRepo
{
    public function store(StoreCommentRQ $data): Comment;

    public function update(int $id, array $data): bool;

    public function delete(int $id): bool;

    public function getTree(): object;
}
