<?php

namespace Comments\Entities;

interface Comment
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getAuthor(): string;

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void;

    /**
     * @return string
     */
    public function getText(): string;

    /**
     * @param string $text
     */
    public function setText(string $text): void;

    /**
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * @return string
     */
    public function getUpdatedAt(): string;
}
