<?php

namespace Comments\Entities\Requests;

class StoreCommentRQ
{
    /** @var string */
    private $text;

    /** @var string */
    private $author;

    /** @var int|null */
    private $parentId;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId(?int $parentId): void
    {
        $this->parentId = $parentId;
    }

    public function asArray()
    {
        return [
            'text' => $this->getText(),
            'author' => $this->getAuthor(),
            'parent_id' => $this->getParentId(),
        ];
    }
}
