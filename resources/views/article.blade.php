<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Комментарии</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            .author {
                font-size:18px;
                font-weight: bold;
            }

            .label {
                font-size: 14px;
                color: grey;
                cursor: pointer;
            }

            .show_answers {
                font-size: 14px;
                color: steelblue;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="comments" class="offset-3 col-6 ">
                </div>
            </div>
            <div class="row mt-3">
                <div class="offset-3 col-6">
                    <form id="sendForm">
                        <div class="form-group">
                            <input name="author" placeholder="Author" type="text" class="form-control mb-2">
                            <div class="input-group mb-3">
                                <textarea name="text" placeholder="Text" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit" onclick="sendComment()">></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="replyModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="replyModalLabel">Reply</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="replyForm">
                            <div class="input-group mb-3">
                                <input name="parentId" type="text" hidden>
                                <input name="author" type="text" class="form-control" placeholder="Author">
                                <input name="text" type="text" class="form-control" placeholder="Text">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="replyComment()">Reply</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="changeModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="changeModalLabel">Change</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="changeForm">
                            <div class="input-group mb-3">
                                <input name="id" type="text" hidden>
                                <input name="text" type="text" class="form-control" placeholder="Text">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateComment()">Change</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            const Comment = ({ id, parentId, depth, author, text, children, createdAt, subContent }) => `
                <div class="media mt-2 ml-3" data-id="${id}" data-parent="${parentId}" ${depth === 3 && !showedAnswers.includes(parentId) ? 'hidden' : ''}>
                    <div class="media-body">
                        <div class="mt-0 mb-1 author">${author}</div>
                        <span class="dashboard">
                            <span class="text" data-text-id="${id}">${text}</span>
                            <span class="label" onclick="insertReply(${id})">Reply</span>
                            ${hourDifference(new Date(createdAt), new Date()) < 1 ? `
                                <span class="label" onclick="insertChange(${id}, '${text}')">Change</span>
                                <span class="label" onclick="removeComment(${id})">Remove</span>` : ''}
                        </span>
                        <span class="sub_content">
                            ${subContent}
                        </span>
                        ${depth === 2 && children.length !== 0 && !showedAnswers.includes(id) ? `<div class="media show_answers" onclick="showAnswers(${id})">show answers</div>` : ''}
                    </div>
                </div>`;

            const showedAnswers = [];

            function showAnswers(parentId) {
                showedAnswers.push(parentId);
                $(`div[data-id='${parentId}']`).find('.show_answers').remove();
                $(`div[data-parent='${parentId}']`).removeAttr('hidden');
            }

            function insertReply(parentId) {
                let modal = $('#replyModal');
                modal.find("input[name='parentId']").val(parentId);
                modal.find("input[name='author']").val('');
                modal.find("input[name='text']").val('');
                modal.modal('show');
            }

            function insertChange(id, text) {
                let modal = $('#changeModal');
                modal.find("input[name='id']").val(id);
                modal.find("input[name='text']").val(text);
                modal.modal('show');
            }

            function hourDifference(date1, date2) {
                return Math.abs(date1 - date2) / 36e5;
            }

            function buildCommentsTree(comments) {
                let content = '';
                for (let i = 0; i < comments.length; i++) {
                    let subContent = buildCommentsTree(comments[i].children);
                    content += Comment({
                            id:comments[i].id,
                            parentId:comments[i].parentId,
                            depth:comments[i].depth,
                            author:comments[i].author,
                            text:comments[i].text,
                            children:comments[i].children,
                            createdAt:comments[i].createdAt,
                            subContent:subContent
                    });
                }

                return content;
            }

            function removeComment(id) {
                apiRemoveComment(id);
                $(`div[data-id='${id}']`).remove();
            }

            function updateComment() {
                const form = $('#changeForm');
                const id = form.find("input[name='id']").val();
                apiUpdateComment(id, form.serialize());
                $(`div[data-id='${id}']`).find('.text').eq(0).text(form.find('input[name="text"]').val());
            }

            function replyComment() {
                const form = $('#replyForm');
                let response = apiSendComment(form.serialize());
                let content = Comment({
                    id:response.id,
                    parentId:response.parentId,
                    depth:response.depth,
                    author:response.author,
                    text:response.text,
                    children:response.children,
                    createdAt:response.createdAt,
                    subContent:''
                });

                showAnswers(response.parentId);
                $(`div[data-id='${response.parentId}']`).find('.sub_content').eq(0).append(content);
            }

            function sendComment() {
                const form = $('#sendForm');
                form.submit(function(e){return false;});
                let response = apiSendComment(form.serialize());
                let content = Comment({
                    id:response.id,
                    parentId:response.parentId,
                    depth:response.depth,
                    author:response.author,
                    text:response.text,
                    children:response.children,
                    createdAt:response.createdAt,
                    subContent:''
                });
                $(content).appendTo('div#comments')
            }

            function apiRemoveComment(id) {
                return JSON.parse($.ajax({
                        type: 'DELETE',
                        url: `http://127.0.0.1:8000/api/comments/${id}`,
                        async: false,
                    }).responseText
                );
            }

            function apiUpdateComment(id, data) {
                return JSON.parse($.ajax({
                        type: 'PUT',
                        url: `http://127.0.0.1:8000/api/comments/${id}`,
                        async: false,
                        data: data,
                    }).responseText
                );
            }

            function apiSendComment(data) {
                return JSON.parse($.ajax({
                        type: 'POST',
                        url: "http://127.0.0.1:8000/api/comments",
                        async: false,
                        data: data,
                    }).responseText
                ).data;
            }

            function apiGetComments() {
                return JSON.parse(
                    $.ajax({
                        url: "http://127.0.0.1:8000/api/comments",
                        async: false
                    }).responseText
                ).data;
            }

            function getCommentTree() {
                let comments = apiGetComments();
                let content = buildCommentsTree(comments);
                $("div#comments").html(content);
            }

            $(document).ready(function() {
                getCommentTree();
                setInterval( getCommentTree , 5000 );
            });
        </script>


    </body>
</html>
