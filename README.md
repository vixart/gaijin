### Развертывание проекта

- Клонируем проект git clone git@gitlab.com:vixart/gaijin.git project
- composer install
- npm install && npm run dev
- cp .env.example .env
- изменить подключение к бд в .env
- php artisan migrate
- php artisan key:generate
- php artisan serve
